package ingredients

import "github.com/schollz/ingredients"

type IngredientsBackend interface {
	FromHTMLStr(string) (Ingredients, error)
	FromHTMLURL(string) (Ingredients, error)
}

type SchollzBackend struct{}

var Schollz = SchollzBackend{}

func (s SchollzBackend) FromHTMLStr(str string) (Ingredients, error) {
	i, err := ingredients.NewFromString(str)
	if err != nil {
		return Ingredients{}, err
	}
	listRaw := i.Ingredients
	list := []Ingredient{}
	for _, raw := range listRaw {
		list = append(list, Ingredient{
			Name: raw.Name,
			Measure: Measure{
				Name:   raw.Measure.Name,
				Amount: raw.Measure.Amount,
				Cups:   raw.Measure.Cups,
				Weight: raw.Measure.Weight,
			},
			Notes: raw.Comment,
		})
	}
	return list, nil
}

func (s SchollzBackend) FromHTMLURL(URL string) (Ingredients, error) {
	i, err := ingredients.NewFromURL(URL)
	if err != nil {
		return Ingredients{}, err
	}
	listRaw := i.Ingredients
	list := []Ingredient{}
	for _, raw := range listRaw {
		list = append(list, Ingredient{
			Name: raw.Name,
			Measure: Measure{
				Name:   raw.Measure.Name,
				Amount: raw.Measure.Amount,
				Cups:   raw.Measure.Cups,
				Weight: raw.Measure.Weight,
			},
			Notes: raw.Comment,
		})
	}
	return list, nil
}

type Ingredients []Ingredient

type Ingredient struct {
	Name    string  `json:"name"`
	Measure Measure `json:"measure"`
	Notes   string  `json:"notes"`
}

type Measure struct {
	Name   string  `json:"name"`
	Amount float64 `json:"amount"`
	Cups   float64 `json:"cups"`
	Weight float64 `json:"weight"`
}
