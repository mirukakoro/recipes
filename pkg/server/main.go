package server

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

func Run() error {
	r := gin.New()
	r.Use(cors.Default())
	r.Use(gin.Logger())
	r.LoadHTMLGlob("tmpl/*.tmpl.html")
	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.tmpl.html", nil)
	})
	r.GET("/api/ingr/*addr", APIIngrAddr)
	return r.Run()
}

func RunGAE() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}
	r := gin.New()
	r.Use(cors.Default())
	r.Use(gin.Logger())
	r.LoadHTMLGlob("tmpl/*.tmpl.html")
	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.tmpl.html", nil)
	})
	r.GET("/api/ingr/*addr", APIIngrAddr)
	log.Fatal(r.Run(fmt.Sprintf(":%v", port)))
}
